package library

import (
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

//----------------------------------------------------------------------
// http.404
//-----------------------------------------------------------------------
func (s *Server) Handler404(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	_ = s.R.RenderCode(http.StatusNotFound, "404.html", ses, w)
}

//----------------------------------------------------------------------
// http.405
//-----------------------------------------------------------------------
func (s *Server) Handler405(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	_ = s.R.RenderCode(http.StatusMethodNotAllowed, "405.html", ses, w)
}

//----------------------------------------------------------------------
// root
//-----------------------------------------------------------------------
func (s *Server) HandlerRoot(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// root
//-----------------------------------------------------------------------
func (s *Server) HandlerFrllo(w http.ResponseWriter, r *http.Request) {
	ses := s.getContext(r)
	if err := s.R.Render("index.html", ses, w); err != nil {
		s.log.Println(err)
	}
}

//----------------------------------------------------------------------
// websocket :: FRLLO
//-----------------------------------------------------------------------
// make new websocket instanse
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (s *Server) WebsocketHandler(w http.ResponseWriter, r *http.Request) {
	//переменные
	var chanTOTALBREAK = make(chan int)  // канал для прерывания работы запущенных воркеров
	var command = make(chan string)      // управляющий канал для монопольного писателя в вебсокет
	var toWrite = make(chan interface{}) // канал для передачи данных, какие надо передать в вебсо
	var countWorkers = 0                 //количество работающих задача в текущем вебсокете

	//создаю коннектор для обработки websocketa
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		s.log.Println(err)
		return
	}
	s.log.Println("connection established from client")

	//создаю и запускай горутина для монопольной записи в вебсокет
	go s.WriterWebsocket(conn, toWrite, command)

	//дефер функция
	defer func() {
		s.log.Printf("[DEFER fUNCTION CLOSED WEB HANDLER]\n")
		err := conn.Close()
		if err != nil {
			s.log.Printf("error close websocket `%v`\n", err)
		}
		//отправка команды горутинам на выход //при обрыве websocket соединения
		chanTOTALBREAK <- 0
	}()

	//бесконечный цикл для обработки входящих от клиента и отправка ему ответов
	for {
		if conn == nil {
			s.log.Printf("[WARNING-ERROR] WEBSOCKET IS CLOSED!!!!!\n")
		} else {
			//читаю с клиента
			var fromClient FromClient
			err = conn.ReadJSON(&fromClient)
			if err != nil {
				//TODO: добавить анализ ошибок + добавить логику
				s.log.Printf("[error read JSON from client] %v %v\n", err, fromClient)
				return
			} else {
				switch fromClient.Command {
				case "runfrllo_default":
					tt := time.Now()
					s.log.Printf("COMMAND FROM WEBFACE::: =  %v %v %v\n", fromClient.DateEnd, fromClient.DateStart, tt.Format("2006-01-02"))

				case "runfrllo":
					s.log.Printf("COMMAND FROM WEBFACE::: =  %v\n", fromClient)
					toWrite <- TOClient{
						Command: "ok_runscriptcommand",
						Data:    "some data foir testing post",
					}

					//команда от вебморды о прекращении работы конкретной проверки
				case "kill":
					s.log.Printf("====KILL COMMAND FROM GUID => `%v`\n", fromClient.Command)

					//отправляю всем менеджерам команду
					for x := 0; x < countWorkers; x++ {
						command <- fromClient.Command
					}
					s.log.Printf("====KILL COMMAND FROM GUID => `%v` [MASS SENDED] \n", fromClient.Command)

				default:
					s.log.Printf("wrong command from client\n")
				}
			}
		}
	}
}
