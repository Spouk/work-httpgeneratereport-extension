package library

import (
	"time"
)

type (
	//----------------------------------------------------------------------
	// конфигурация
	//-----------------------------------------------------------------------
	ConfigStruct struct {
		//template
		TemplatePath       string        `yaml:"templatepath"`
		TemplateDebug      bool          `yaml:"templatedebug"`
		TemplateDebugFatal bool          `yaml:"teamplatedebugfatal"`
		ReadTimeout        time.Duration `yaml:"readtimeout"`
		WriteTimeout       time.Duration `yaml:"writetimeout"`

		//key for switch developing/production mode
		Debugcheckdb bool `yaml:"debugcheckdb"`
		Developing   bool `yaml:"developing"`

		//serverjs developing
		ServerJSDEV   string `yaml:"serverjsdev"`
		ServerJSWSDEV string `yaml:"serverjswsdev"`

		//serverjs
		ServerJS   string `yaml:"serverjs"`
		ServerJSWS string `yaml:"serverjsws"`

		//server
		AdressHTTP            string `yaml:"adresshttp"`
		RedirectTrailingSlash bool   `yaml:"redirecttrailingslash"`
		RedirectFixedPath     bool   `yaml:"redirectfixedpath"`

		//Logging -> статичный сегмент
		Logfile      string `yaml:"logfile"`
		LogTagsyslog string `yaml:"logtagsyslog"`
		LogPrefix    string `yaml:"logprefix"`

		//files
		StaticPath     string        `yaml:"staticpath"`
		StaticPrefix   string        `yaml:"staticprefix"`
		FileTimerSleep time.Duration `yaml:"filetimersleep"`
		UploadPath     string        `yaml:"uploadpath"`
	}
	//----------------------------------------------------------------------
	// ajax
	//-----------------------------------------------------------------------
	Answer struct {
		Stage string             `json:"stage"` //статус событийной цепочки - начало, в процессе, конечный результат
		T     []TesterAnswerAjax `json:"t"`
	}
	TesterAnswerAjax struct {
		IDS    int    `json:"ids"`    // идентификатор горутины
		Name   string `json:"name"`   // имя горутины
		URL    string `json:"url"`    //url запроса
		Status string `json:"status"` //статус запроса (start, success, fail, not response)
	}
	//----------------------------------------------------------------------
	// websocket
	//-----------------------------------------------------------------------
	FromClient struct {
		Status    string `json:"status"`
		Command   string `json:"command"`
		DateStart string `json:"datestart"`
		DateEnd   string `json:"dateend"`
	}
	TOClient struct {
		Command string `json:"command"`
		Data    string `json:"worker" `
	}
)
