package library

import (
	"github.com/gorilla/websocket"
)

//----------------------------------------------------------------------
// общая горутина, которая будет заниматься обработкой всей записи в вебсокет
// единоличная запись  в контексте внешнего вызова, данная горутина не читает вебсокет
// чтением занимается вебхэндлер
// toRead - канал для чтения
// toWrite - канал для записи
// command - управляющий канал
//-----------------------------------------------------------------------
func (s *Server) WriterWebsocket(w *websocket.Conn, toWrite chan interface{}, command chan string) {
	s.log.Println("[WriterWebsocket] starting...")
	defer func() {
		s.log.Println("[WriterWebsocket] exit...")
	}()
	for {
		select {
		//канал для  получения структур для записи в вебсокет извне
		case some := <-toWrite:
			err := w.WriteJSON(some)
			if err != nil {
				s.log.Printf("[WriterWebsocket] [error] %v\n", err)
			}
			//управляющий канал
		case <-command:
			return
		}
	}
}
