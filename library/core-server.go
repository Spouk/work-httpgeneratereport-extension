package library

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/nakagami/firebirdsql"
	"gitlab.com/Spouk/gotool/config"
	"gitlab.com/Spouk/gotool/convert"
	"gitlab.com/Spouk/gotool/render"
	"gitlab.com/Spouk/gotool/session"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

const (
	logPrefixServer  = "[work-web-portal-log]"
	logServerBitMask = log.LstdFlags | log.Lshortfile
)

type Server struct {
	Mux     *chi.Mux
	log     *log.Logger
	R       *render.Render
	cfg     *ConfigStruct
	Pool    sync.Pool
	Convert *convert.Convert
}

//создание нового инстанса
func NewServer(configFile string) *Server {

	//читаю конфигурационный файл
	cf := ConfigStruct{}
	c := config.NewConf("", os.Stdout)
	err := c.ReadConfig(configFile, &cf)
	if err != nil {
		log.Fatal(err)
	}

	//создаю инстанс сервера
	var s = &Server{
		log: log.New(os.Stdout, logPrefixServer, logServerBitMask),
		R:   render.NewRender(cf.TemplatePath, cf.TemplateDebug, nil, cf.TemplateDebugFatal),
		Mux: chi.NewMux(),
		cfg: &cf,
	}

	//определение пула для сессий
	s.Pool = sync.Pool{
		New: func() interface{} {
			return session.New()
		},
	}

	//добавляю миддлы
	s.Mux.Use(middleware.Logger)
	s.Mux.Use(middleware.Recoverer)
	s.Mux.Use(middleware.StripSlashes)
	s.Mux.Use(s.MiddlewareSession)

	//статичные файлы
	s.Mux.Route("/static/", func(root chi.Router) {
		//формирую данные исходя из конфига
		//workDir, _ := os.Getwd()
		//c.Log.Printf("GETWD: %v\n", workDir)
		filesDir := filepath.Join(s.cfg.StaticPath, "static")
		s.log.Printf("===> STATIC PATH: %v\n", filesDir)
		s.FileServer(root, "/static", "/", http.Dir(filesDir))
	})

	//создаю конвертер
	s.Convert = convert.NewConverter(s.log)

	//404Error
	s.Mux.NotFound(s.Handler404)

	//405Error
	s.Mux.MethodNotAllowed(s.Handler405)

	//возврат инстанса
	return s
}

//статичные файлы
func (s *Server) FileServer(r chi.Router, basePath string, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(basePath+path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

//запуск сервера без сертификатов
func (s *Server) Run() {
	s.log.Printf("starting server on `%s`\n", s.cfg.AdressHTTP)
	s.log.Fatal(http.ListenAndServe(s.cfg.AdressHTTP, s.Mux))
}

func (s *Server) getContext(r *http.Request) *session.Session {
	ses, foundSession := r.Context().Value("session").(*session.Session)
	if foundSession {
		//добавление в сессию нужных переменных
		ccc := chi.RouteContext(r.Context())

		//текстовые
		ses.SetTEXT("request", "routepath", ccc.RoutePath)
		ses.SetTEXT("request", "routemethod", ccc.RouteMethod)
		ses.SetTEXT("request", "routepatern", ccc.RoutePattern())
		ses.SetTEXT("request", "urlparams", ccc.URLParams)

		//остальное
		ses.SetDATA("stock", "config", s.cfg)
		ses.SetDATA("stock", "command", ccc.URLParam("command"))
		ses.SetDATA("stock", "id", ccc.URLParam("id"))
		ses.SetDATA("stock", "idint", s.Convert.DirectStringtoInt(ccc.URLParam("id")))

	} else {
		s.log.Println("ОШИБКА -- СЕССИЯ НЕ НАЙДЕНА В ТЕКУЩЕМ КОНТЕКСТЕ\n")
	}
	return ses
}
