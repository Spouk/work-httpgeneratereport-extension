/////////////////////////////////////////////////////////
// websocket client part for communication  switch server and client
///////////////////////////////////////////////////////////
class WebsockFRRLO {
    constructor(serverAddr) {
        this.connectIcon = document.getElementById('connicon');
        console.log('result.webconnection => ', this.connectIcon);
        this.socket = new WebSocket(serverAddr);

        //OPEN->WS
        this.socket.onopen = function (e) {
            console.log('[socket] connection established');
            console.log('[socket] send data to server');
            document.getElementById('connicon').classList.add("error_green");
        };


        //READ->WS ( поступление сообщения от сервера )
        this.socket.onmessage = function (e) {
            //парсинг блока данных
            let res = JSON.parse(e.data);
            console.log("------> ", res);
            //выстраиваю логическое дерево
            switch (res.command) {
                // //установки информационного значения
                // case "infoset":
                //     let block = document.getElementById(idblock);
                //     block.innerText = res.client.nodes;
                //     console.log("FROM CLIENT: ", res);
                //     break;
                // //количество отработавших горутин
                // case "count":
                //     console.log("COUNT---- ",res);
                //     //добавляю инфу о количестве проверенных серверов
                //     document.getElementById(checked).innerText = res.client.count_check;
                //     break;

                //подтверждение получения команды от вебморды на запуск скрипта
                case "ok_runscriptcommand":
                        console.log("--> from server::: ", res.worker);
                        //меняю статус кнопки отпавки события на сервер
                        setTimeout(()=> {}, 5);
                        let btn = document.getElementById('runscript');
                        btn.disabled = false ;
                        btn.innerText = 'проверить все сервера';
                        break;

                //одиночная проверка
                case "work":
                    if (res.worker.result === false) {
                        //пробую получить блок с данными по IDS
                        let block = document.getElementById(res.worker.ids);

                        if (block === null) {
                            //блок не найден, создаю
                            let coreS = document.getElementById(core);
                            let newdiv = document.createElement('div');
                            newdiv.classList.add('box');
                            newdiv.id = res.worker.ids;

                            let button = document.createElement('div');
                            button.classList.add('button');
                            button.classList.add('is-primary');

                            let fu2 = function (a, b, e) {
                                console.log(a, b, e);
                                console.log("RESS --- > ", b);
                                let MsgServer = {
                                    Command: "kill",
                                    Marked: res.worker.marked,
                                };
                                a.send(JSON.stringify(MsgServer));
                                document.getElementById(b.worker.ids).remove();
                            }

                            //добавляю обработчик на кнопку для прекращения работы обработчика
                            // button.addEventListener("click", () => fu(res, this.socket, e));
                            // button.addEventListener("click", () => fu2(this.socket, res, e));
                            button.addEventListener("click", event => {
                                this.CloseWorker(event)
                            });


                            let infobox = document.createElement('div');
                            infobox.classList.add('box');
                            infobox.id = res.worker.ids + "infobox";

                            let logbox = document.createElement('div');
                            logbox.classList.add('box');
                            logbox.id = res.worker.ids + "logbox";

                            let text = document.createElement('textarea');
                            text.classList.add('textarea');
                            text.id = res.worker.ids + "text";

                            logbox.appendChild(text);
                            newdiv.appendChild(infobox);
                            newdiv.appendChild(button);
                            newdiv.appendChild(logbox);
                            coreS.appendChild(newdiv);


                        } else {
                            //блок найден, обновляю данные
                            let infobox = document.getElementById(res.worker.ids + "infobox");
                            let text = document.getElementById(res.worker.ids + "text");
                            infobox.innerHTML = "";
                            infobox.innerHTML = "=> " + res.worker.node + " run: " + res.worker.time_run;
                            text.value += "\n";
                            text.value += res.worker.result + " " + res.worker.error + " " + res.worker.timerun;
                        }
                    }
                    break;

                //постоянная проверка, с заданной периодичностью
                case "end":
                    //изменяю статус кнопки и делаю ее доступной
                    let bb = document.getElementById(btn);
                    bb.disabled = false;
                    bb.innerText = "проверить сервера";
                    //изменяю статус кнопки останов
                    let b = document.getElementById(btnStop);
                    b.disabled = true;

                    console.log("проверка закончена ", res);

                    //добавляю инфу о количестве проверенных серверов
                    document.getElementById(checked).innerText = res.client.count_check;

                    break;
                default:
                    console.log("wrong  res.client.command", res.client.command);
                    break;
            }
        };

        //соединеие закрыто
        this.socket.onclose = function (e) {
            if (e.wasClean) {
                console.log(`[socket] соединение закрыто без ошибок ${e.code}   ${e.reason}`);
            } else {
                console.log('[socket] соединение закрыто со стороны сервера ', e.code);
            }
            document.getElementById(connIcon).classList.add("error_red");
        };

        //ошибка
        this.socket.onerror = function (e) {
            console.log(`[socket] websocket error ${e.message}`);
        };

    }


    CloseWorker(res, ids, e) {
        console.log("ETHIS: ", e.this);
        console.log("E: ", e);
        console.log("ids: ", ids);
        console.log("res: ", res);
        // //отправить команду во вебсокету на прекращение работы этого обработчика
        // let MsgServer = {
        //     Command: "kill",
        //     Marked: res.worker.marked,
        // };
        // console.log("MSgServer=killaddeventlistener: ", MsgServer);
        // //отправляю на сервер команду на запуск проверки
        // this.socket.send(JSON.stringify(MsgServer));
        // //убираем бокс по этой горутине
        // document.getElementById(res.worker.ids).remove();
    };

    //функция отправки команды на запуск приложения
    RunScript() {
        console.log('[event] run script command\n')
        //изменяю статус кнопки
        let btn = document.getElementById('runscript');
        btn.disabled = true;
        btn.innerText = "в обработке..."
        //make msg to server
        let MsgServer = {
            Command: "runfrllo",
        };
        console.log("=> MSgServer: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }


    //функция отправки команды на запуск приложения
    RunDefault() {
        console.log('[event] run default\n')
        //изменяю статус кнопки
        let btn = document.getElementById('run default');
        //получаю данные из форм времени
        let defStart = document.getElementById('defStart');
        let defEnd = document.getElementById('defEnd');
        console.log('Defstart: ', defStart);

        // btn.disabled = true;
        // btn.innerText = "в обработке..."
        //make msg to server
        let MsgServer = {
            Command: "runfrllo_default",
            DateStart: defStart.value,
            DateEnd: defEnd.value,
        };
        console.log("=> MSgServer defblock: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }


    //функция создания блока под горутину
    MakeForm(core, ids) {
        let coreS = document.getElementById(core);
        let newdiv = document.createElement('div');
        newdiv.classList.add('box');
        let infobox = document.createElement('div');
        infobox.classList.add('box');
        let logbox = document.createElement('div');
        logbox.classList.add('box');
        newdiv.appendChild(infobox);
        newdiv.appendChild(logbox);
        coreS.appendChild(newdiv);
    }

    //кнопка отправки данных на сервер из формы создания обработчика
    SendForm(e) {
        //get form
        let cform = document.forms.noda;
        console.log("Form: ", cform);
        //make msg to server
        let MsgServer = {
            Command: "new",
            Dbs: cform.elements.db.value,
            Period: cform.elements.period.value,
            Timeout: cform.elements.timeout.value,
        };
        console.log("MSgServer: ", MsgServer);
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(MsgServer));
    }


    //вешать на кнопку старт
    startEvent(t) {
        //получаю кнопку-останов
        let bb = document.getElementById(this.btnstop);

        //очищаю предыдущий результат по списку
        let elemList = document.getElementById(this.list);
        console.log("Element list: ", elemList);
        elemList.innerHTML = "";
        elemList.innerText = "";
        console.log("[after] Element list: ", elemList);

        //добавляю инфу о количестве проверенных серверов
        document.getElementById(this.checked).innerText = 0;

        //получюа поле с чекбоксом
        let box = document.getElementById(this.single);
        console.log("BOX CHECKED : ", box.checked);
        //при выставленном чекбоксом идет одиночная проверка, без - проверка по таймеру
        let startStruct = {
            Status: "enable",
            Command: ""
        };
        if (box.checked) {
            startStruct.Command = "start_single";
        } else {
            startStruct.Command = "start_period";
        }

        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(startStruct));

        //получаю кнопку для изменения
        t.innerText = "в работе...";
        t.disabled = true;

        //активирую кнопку-останов
        bb.disabled = false;
    }

    //кнопка останов
    stopEvent(e) {
        let bb = document.getElementById(this.btnStop);
        //проверка рабочей версии - одиночной или по таймеру
        let btn = document.getElementById(this.single);
        let ss = {
            Status: "enable",
            Command: ""
        };
        if (btn.checked) {
            ss.command = "stop_single";

        } else {
            ss.command = "stop_period";
        }
        //отправляю на сервер команду на запуск проверки
        this.socket.send(JSON.stringify(ss));
    }

}
