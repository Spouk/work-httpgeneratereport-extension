package main

import (
	"gitlab.com/work-httpgeneratereport-extension/library"
)

func main() {
	cfg := "/home/spouk/stock/s.develop/go/src/gitlab.com/work-httpgeneratereport-extension/config.yaml"
	s := library.NewServer(cfg)

	//root
	s.Mux.Get("/", s.HandlerRoot)

	//frllo
	s.Mux.Get("/frllo", s.HandlerFrllo)
	s.Mux.Post("/frllo", s.HandlerFrllo)
	s.Mux.Get("/frllo/{command}", s.HandlerFrllo)
	s.Mux.Post("/frllo/{command}", s.HandlerFrllo)
	s.Mux.Get("/frllo/{command}/{id}", s.HandlerFrllo)
	s.Mux.Post("/frllo/{command}/{id}", s.HandlerFrllo)

	//websocket :: FRLLO
	s.Mux.Get("/ws/frllo", s.WebsocketHandler)
	s.Mux.Post("/ws/frllo", s.WebsocketHandler)

	//run server
	s.Run()
}
